<?php

header('Content-Type: application/json');

include '../dbConnection.php';

$response = array();

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    if (isset($_GET['id'])) {
        $id = $_GET['id'];
        
        if ($id === 'all') {
            $sql = "SELECT * FROM users";
            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
                $users = array();
                while ($userRow = $result->fetch_assoc()) {
                    $userId = $userRow['id'];
                    $user = array(
                        'id' => $userId,
                        'username' => $userRow['username'],
                        'listings' => array()
                    );

                    $listingsSql = "SELECT * FROM listings WHERE seller = $userId";
                    $listingsResult = $conn->query($listingsSql);

                    if ($listingsResult->num_rows > 0) {
                        while ($listingRow = $listingsResult->fetch_assoc()) {
                            $user['listings'][] = array(
                                'id' => $listingRow['id'],
                                'NAME' => $listingRow['NAME'],
                                'image' => $listingRow['image'],
                                'price' => $listingRow['price'],
                                'location' => $listingRow['location']
                            );
                        }
                    }

                    $users[] = $user;
                }

                $response['status'] = 'success';
                $response['data'] = $users;
            } else {
                $response['status'] = 'error';
                $response['message'] = 'No users found';
            }
        } else {
            $userId = intval($id);

            $userSql = "SELECT * FROM users WHERE id = $userId";
            $userResult = $conn->query($userSql);

            if ($userResult->num_rows > 0) {
                $userRow = $userResult->fetch_assoc();
                $user = array(
                    'id' => $userRow['id'],
                    'username' => $userRow['username'],
                    'listings' => array()
                );

                $listingsSql = "SELECT * FROM listings WHERE seller = $userId";
                $listingsResult = $conn->query($listingsSql);

                if ($listingsResult->num_rows > 0) {
                    while ($listingRow = $listingsResult->fetch_assoc()) {
                        $user['listings'][] = array(
                            'id' => $listingRow['id'],
                            'NAME' => $listingRow['NAME'],
                            'description' => $listingRow['description'],
                            'image' => $listingRow['image'],
                            'location' => $listingRow['location'],
                            'price' => $listingRow['price'],
                            'category' => $listingRow['category']
                        );
                    }
                }

                $response['status'] = 'success';
                $response['data'] = $user;
            } else {
                $response['status'] = 'error';
                $response['message'] = 'User not found';
            }
        }
    } else {
        $response['status'] = 'error';
        $response['message'] = 'Missing id parameter';
    }
} else {
    $response['status'] = 'error';
    $response['message'] = 'Invalid request method';
}

echo json_encode($response);
?>