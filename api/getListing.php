<?php

header('Content-Type: application/json');

include '../dbConnection.php';

$response = array();

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    if (isset($_GET['id'])) {
        $id = $_GET['id'];
        
        if ($id === 'all') {
            $sql = "SELECT * FROM listings";
            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
                $listings = array();
                while ($row = $result->fetch_assoc()) {
                    $listings[] = $row;
                }
                $response['status'] = 'success';
                $response['data'] = $listings;
            } else {
                $response['status'] = 'error';
                $response['message'] = 'No listings found';
            }
        } else {
            $listingId = intval($id);

            $stmt = $conn->prepare("SELECT * FROM listings WHERE id = ?");
            $stmt->bind_param("i", $listingId);
            $stmt->execute();
            $result = $stmt->get_result();

            if ($result->num_rows > 0) {
                $listing = $result->fetch_assoc();
                $response['status'] = 'success';
                $response['data'] = $listing;
            } else {
                $response['status'] = 'error';
                $response['message'] = 'Listing not found';
            }
            $stmt->close();
        }
    } else {
        $response['status'] = 'error';
        $response['message'] = 'Missing id parameter';
    }
} else {
    $response['status'] = 'error';
    $response['message'] = 'Invalid request method';
}

echo json_encode($response);
?>
