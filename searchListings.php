<?php
session_start();
include 'dbConnection.php';

$category = isset($_GET['category']) ? $_GET['category'] : '';
$minPrice = isset($_GET['min_price']) ? $_GET['min_price'] : '';
$maxPrice = isset($_GET['max_price']) ? $_GET['max_price'] : '';
$name = isset($_GET['name']) ? $_GET['name'] : '';

$sql = "SELECT id, name, image, price, location FROM listings WHERE 1=1";
if ($category) {
    $sql .= " AND category = '$category'";
}
if ($minPrice) {
    $sql .= " AND price >= '$minPrice'";
}
if ($maxPrice) {
    $sql .= " AND price <= '$maxPrice'";
}
if ($name) {
    $sql .= " AND name LIKE '%$name%'";
}

$listings = $conn->query($sql);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/x-icon" href="images/icon.png">
    <title>Repalo</title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    
    <?php include 'navbar.php'; ?>

    <div class="w3-row" style="margin-top: 65px;">
        <div class="w3-col SideBar"></div>
        <div class="w3-col Content">
            <div class="listingContainer">
                <h2>Oglasi</h2>
                <!-- Search Form -->
                <form method="GET" action="searchListings.php">
                    <label for="category">Kategorija:</label>
                    <select id="category" name="category">
                        <option value="">--Odaberi kategoriju--</option>
                        <option value="Auto-Moto">Auto-Moto</option>
                        <option value="Knjige">Knjige</option>
                        <option value="Odjeća">Odjeća</option>
                        <option value="Nekretnine">Nekretnine</option>
                        <option value="Informatika">Informatika</option>
                        <option value="Ostalo">Ostalo</option>
                    </select>
                    <br>
                    <label for="min_price">Minimalna cijena:</label>
                    <input type="number" id="min_price" name="min_price" min="0" value="<?php echo htmlspecialchars($minPrice); ?>">
                    <br>
                    <label for="max_price">Maksimalna cijena:</label>
                    <input type="number" id="max_price" name="max_price" min="0" value="<?php echo htmlspecialchars($maxPrice); ?>">
                    <br>
                    <label for="name">Naziv:</label>
                    <input type="text" id="name" name="name" value="<?php echo htmlspecialchars($name); ?>">
                    <br>
                    <button type="submit" class="w3-button w3-round-medium w3-blue">Pretraži</button>
                </form>
                <!-- Listings -->
                <div class="listings">
                    <?php if ($listings->num_rows > 0): ?>
                        <?php while($row = $listings->fetch_assoc()): ?>
                            <a href="listing.php?id=<?php echo $row['id']; ?>">
                                <div class="listing">
                                    <img src="<?php echo $row['image']; ?>">
                                    <div class="listingDescription">
                                        <h2><?php echo htmlspecialchars($row['name']); ?></h2>
                                        <p><?php echo htmlspecialchars($row['location']); ?></p>
                                        <h3><?php echo htmlspecialchars($row['price']); ?>€</h3>
                                    </div>
                                </div>
                            </a>
                        <?php endwhile; ?>
                    <?php else: ?>
                        <p>Nema oglasa.</p>
                    <?php endif; ?>
                </div>
            </div>
            <div class="w3-col SideBar"></div>
        </div>
    </div>
    <script src="modal.js"></script>

    <footer>
        <p>Created by Duka</p>
    </footer>
</body>
</html>

<?php $conn->close(); ?>
