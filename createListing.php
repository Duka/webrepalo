<?php
session_start();
include 'dbConnection.php';

if (!isset($_SESSION['user_id'])) {
    header("Location: index.php");
    exit();
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $name = $_POST['name'];
    $description = $_POST['description'];
    $price = $_POST['price'];
    $seller = $_SESSION['user_id'];
    $location = $_POST['location'];
    $category = $_POST['category'];

    // Insert listing data first
    $sql = "INSERT INTO listings (name, description, price, seller, location, category) VALUES ('$name', '$description', '$price', '$seller', '$location', '$category')";
    if ($conn->query($sql) === TRUE) {
        $listing_id = $conn->insert_id;

        // Handle file upload
        $target_dir = "images/listings/";
        $imageFileType = strtolower(pathinfo($_FILES["fileInput"]["name"], PATHINFO_EXTENSION));
        $target_file = $target_dir . $listing_id . '.' . $imageFileType;

        // Ensure the directory exists
        if (!is_dir($target_dir)) {
            mkdir($target_dir, 0777, true);
        }

        if (move_uploaded_file($_FILES["fileInput"]["tmp_name"], $target_file)) {
            $image_name = $listing_id . '.' . $imageFileType;

            // Update listing with image
            $sql = "UPDATE listings SET image='$target_file' WHERE id=$listing_id";
            if ($conn->query($sql) === TRUE) {
                echo "<script>alert('Listing created successfully'); window.location.href='index.php';</script>";
            } else {
                echo "Error updating record: " . $conn->error;
            }
        } else {
            echo "Sorry, there was an error uploading your file.";
        }
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/x-icon" href="images/icon.png">
    <title>Create Listing</title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="styles.css">
</head>
<body style="margin-top: 65px; background-color: white;">
    
    <?php include 'navbar.php'; ?>

    <div class="listingCreatorContainer">
        <form method="post" action="" enctype="multipart/form-data">
            <label for="fileInput" class="w3-button w3-round-medium w3-button" id="blueButton">Odaberi sliku</label>
            <input type="file" id="fileInput" name="fileInput" accept="image/*" style="display: none;" required>
            <span id="fileNameDisplay"></span>
            <br>
            <label for="name">Ime:</label>
            <br>
            <input type="text" id="name" name="name" required>
            <br>
            <label for="description">Opis:</label>
            <br>
            <textarea id="description" name="description" required></textarea>
            <br>
            <label for="price">Cijena:</label>
            <br>
            <input type="text" id="price" name="price" pattern="\d*" title="Please enter numbers only" required>
            <br>
            <label for="location">Lokacija:</label>
            <br>
            <input type="text" id="location" name="location" required>
            <br>
            <label for="category">Kategorija:</label> <!-- New Category Field -->
            <br>
            <select id="category" name="category" required>
                <option value="Knjige">Knjige</option>
                <option value="Odjeća">Odjeća</option>
                <option value="Auto-Moto">Auto-Moto</option>
                <option value="Nekretnine">Nekretnine</option>
                <option value="Informatika">Informatika</option>
                <option value="Ostalo">Ostalo</option>
            </select>
            <br>
            <button type="submit" class="w3-button w3-round-medium w3-button w3-blue">Predaj oglas</button>
        </form>
    </div>
    
    <footer>
        <p>Created by Duka</p>
    </footer>

    <script>
        document.getElementById('fileInput').addEventListener('change', function() {
            document.getElementById('fileNameDisplay').textContent = this.files[0].name;
        });
    </script>

</body>
</html>

