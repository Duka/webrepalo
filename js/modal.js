const modal = document.querySelector('.modal');
const modalRegister = document.querySelector('.registerModal');
const logInButton = document.querySelector('.logInButton');
const registerButton = document.querySelector('.registerButton');
const modalClose = document.querySelector('.close');
const modalRegisterClose = document.querySelector('.registerClose');

function toggleModal() {
    console.log('Toggling modal');
    modal.classList.toggle('show-modal');
}

function toggleModalRegister() {
    console.log('Toggling register modal');
    modalRegister.classList.toggle('show-modal');
}


if (logInButton)
    logInButton.addEventListener('click', toggleModal);

if (registerButton)
    registerButton.addEventListener('click', toggleModalRegister);


if (modalClose)
    modalClose.addEventListener('click', toggleModal);

if (modalRegisterClose)
    modalRegisterClose.addEventListener('click', toggleModalRegister);

