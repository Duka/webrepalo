const editProfileModal = document.querySelector('.editProfileModal');
const editProfileClose = document.querySelector('.editProfileClose');
const editProfileButton = document.querySelector('.editProfileButton');

function toggleModalEditProfile(){
    console.log('Toggling edit profile modal');
    editProfileModal.classList.toggle('show-modal');
}

if (editProfileButton){
    editProfileButton.addEventListener('click', toggleModalEditProfile)
    console.log("Postojim!")
}
else{
    console.log("Ne postojim!")
}

if (editProfileClose)
    editProfileClose.addEventListener('click', toggleModalEditProfile);
