<?php
session_start();
include 'dbConnection.php';

$sql = "SELECT id, name, image, price, location FROM listings";
$listings = $conn->query($sql);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/x-icon" href="images/icon.png">
    <title>Repalo</title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <?php include 'navbar.php'; ?>

    <div class="w3-row" style="margin-top: 65px;">
        <div class="w3-col SideBar">
        </div>

        <div class="w3-col Content">
            <div class="BackdropImage">
            </div>
            <div style="display: flex; justify-content: center; padding: 15px;" class="indexButtonContainer">
                <div class="upperButtons">
                    <a href="searchListings.php?category=Knjige"><button type="button" class="w3-button w3-round-medium w3-blue categoryButton">Knjige</button></a>
                    <a href="searchListings.php?category=Odjeća"><button type="button" class="w3-button w3-round-medium w3-blue categoryButton">Odjeća</button></a>
                    <a href="searchListings.php?category=Auto-Moto"><button type="button" class="w3-button w3-round-medium w3-blue categoryButton">Auto-Moto</button></a>    
                </div>
                <div class="lowerButtons">
                    <a href="searchListings.php?category=Nekretnine"><button type="button" class="w3-button w3-round-medium w3-blue categoryButton">Nekretnine</button></a>
                    <a href="searchListings.php?category=Informatika"><button type="button" class="w3-button w3-round-medium w3-blue categoryButton">Informatika</button></a>
                    <a href="searchListings.php?category=Ostalo"><button type="button" class="w3-button w3-round-medium w3-blue categoryButton">Ostalo</button></a>
                </div>
            </div>
            <div class="listingContainer">
                <h2>Najnoviji oglasi</h2>
                <div class="listings">
                    <?php 
                    $reversedListings = [];
                    while ($row = $listings->fetch_assoc()) {
                        $reversedListings[] = $row;
                    }
                    $reversedListings = array_reverse($reversedListings);

                    if (count($reversedListings) > 0) {
                        foreach ($reversedListings as $row):
                    ?>
                            <a href="listing.php?id=<?php echo $row['id']; ?>">
                                <div class="listing">
                                    <img src="<?php echo htmlspecialchars($row['image']); ?>">
                                    <div class="listingDescription">
                                        <h2><?php echo htmlspecialchars($row['name']); ?></h2>
                                        <p><?php echo htmlspecialchars($row['location']); ?></p>
                                        <h3><?php echo htmlspecialchars($row['price']); ?>€</h3>
                                    </div>
                                </div>
                            </a>
                    <?php 
                        endforeach;
                    } else { 
                    ?>
                        <p>Nema oglasa.</p>
                    <?php } ?>
                </div>
            </div>
            <div class="w3-col SideBar">
            </div>
        </div>
    </div>

    <footer>
        <p>Created by Duka</p>
    </footer>


</body>
</html>

<?php $conn->close(); ?>