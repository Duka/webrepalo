-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 18, 2024 at 10:05 AM
-- Server version: 10.4.32-MariaDB
-- PHP Version: 8.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `repalo`
--

-- --------------------------------------------------------

--
-- Table structure for table `listings`
--

CREATE TABLE `listings` (
  `id` int(11) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `description` text DEFAULT NULL,
  `seller` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `price` float NOT NULL,
  `category` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `listings`
--

INSERT INTO `listings` (`id`, `NAME`, `description`, `seller`, `image`, `location`, `price`, `category`) VALUES
(3, 'Auto', 'Novi avto! Valja sve vjeruj bratu srđanu, samo 1 evro', '2', 'images/listings/3.jpg', 'Novi Sad', 1, 'Auto-Moto'),
(4, 'El Pendeho - NAJBOLJI FILM IKADA!', 'HITNO BRZO HITNO!', '3', 'images/listings/4.png', 'Osijek', 350, 'Knjige'),
(7, 'Bodyguard Servis', 'Opasan je jako brat, ubije boga u kom god oces', '2', 'images/listings/7.jpg', 'Osijek', 500, 'Ostalo'),
(8, 'Kazeta', 'Jako skupa kazeta', '3', 'images/listings/8.png', 'Osijek', 12, 'Informatika');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `PASSWORD` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `contact` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `PASSWORD`, `image`, `contact`) VALUES
(1, 'admin', '0192023a7bbd73250516f069df18b500', 'admin.jpg', NULL),
(2, '123', '202cb962ac59075b964b07152d234b70', '123.jpg', 'Nemoj me kontaktirat'),
(3, 'Duka', 'bd5931f9df923b08f7984138a66e1116', 'Duka.png', 'Mobitel: 095 891 0390'),
(4, 'heroj', '491a3bd2188062f4d152e4917ed5df4a', 'heroj.jpg', 'Nikako me ne kontaktiraj'),
(5, 'test', '098f6bcd4621d373cade4e832627b4f6', 'test.png', 'test');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `listings`
--
ALTER TABLE `listings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `listings`
--
ALTER TABLE `listings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
