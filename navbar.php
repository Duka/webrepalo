<?php

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['login_submit'])) {
    $login_name = $_POST['login_name'];
    $login_password = md5($_POST['login_password']);

    $sql = "SELECT id FROM users WHERE username = '$login_name' AND password = '$login_password'";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        $_SESSION['user_id'] = $row['id'];
        header("Location: " . $_SERVER['REQUEST_URI']);
        exit();
    } else {
        echo "<script>alert('Invalid username or password');</script>";
    }
}

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['register_submit'])) {
    $register_name = $_POST['register_name'];
    $register_password = md5($_POST['register_password']);
    $register_contact = $_POST['register_contact'];

    $checkSql = "SELECT id FROM users WHERE username = '$register_name'";
    $checkResult = $conn->query($checkSql);

    if ($checkResult->num_rows > 0) {
        echo "<script>alert('Korisničko ime je zauzeto, molimo vas da odaberete drugo.');</script>";
    } else {
        $target_dir = "images/users/";
        $imageFileType = strtolower(pathinfo($_FILES["register_image"]["name"], PATHINFO_EXTENSION));
        $target_file = $target_dir . $register_name . '.' . $imageFileType;

        if (!is_dir($target_dir)) {
            mkdir($target_dir, 0777, true);
        }

        if (move_uploaded_file($_FILES["register_image"]["tmp_name"], $target_file)) {
            $image_name = $register_name . '.' . $imageFileType;

            $sql = "INSERT INTO users (username, password, contact, image) VALUES ('$register_name', '$register_password', '$register_contact', '$image_name')";
            if ($conn->query($sql) === TRUE) {
                echo "<script>alert('Registration successful');</script>";
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }
        } else {
            echo "Sorry, there was an error uploading your file.";
        }
    }
}

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['logout'])) {
    session_unset();
    session_destroy();
    header("Location: " . $_SERVER['REQUEST_URI']);
    exit();
}

?>

<nav class="w3-top">
    <div class="w3-bar w3-blue" id="navBar">
        <div style="float: left;">
            <a href="index.php"><img src="images/logo.png" width="50px"></a>
            <form method="GET" action="searchListings.php" style="display: inline;">
                <input type="search" name="name" class="SearchBar">
            </form>
        </div>
        <div style="float: right; display: flex; flex-direction: row; gap: 1em;">
            <?php if (isset($_SESSION['user_id'])): ?>
                <button type="button" class="w3-button w3-round-medium w3-white " onclick="window.location.href='createListing.php'">Predaj Oglas</button>
                <button type="button" class="w3-button w3-round-medium w3-white " onclick="window.location.href='userInformation.php?id=<?php echo $_SESSION['user_id']; ?>'">Profil</button>
                <form method="POST" action="" style="display: inline; @media  (max-width: 950px) { font-size: 2em; }">
                    <button type="submit" class="w3-button w3-round-medium w3-white logOutButton" name="logout">Odjava</button>
                </form>
            <?php else: ?>
                <button type="button" class="w3-button w3-round-medium w3-white registerButton">Registracija</button>
                <button type="button" class="w3-button w3-round-medium w3-white logInButton">Prijava</button>
            <?php endif; ?>
        </div>
    </div>
</nav>

<div class="modal">
    <div class="modal-content">
        <span class="close">&times;</span>
        <div>
            <form method="post" action="">
                <label for="login_name">Korisničko ime:</label>
                <br>
                <input type="text" id="login_name" name="login_name">
                <br>
                <label for="login_password">Zaporka:</label>
                <br>
                <input type="password" id="login_password" name="login_password">
                <br>
                <button class="w3-button w3-round-medium w3-blue" type="submit" name="login_submit">Log in</button>
            </form>
        </div>
    </div>
</div>
<div class="registerModal">
    <div class="modal-content">
        <span class="registerClose">&times;</span>
        <div>
            <form method="post" action="" enctype="multipart/form-data">
                <label for="register_name">Username:</label>
                <br>
                <input type="text" id="register_name" name="register_name">
                <br>
                <label for="register_password">Password:</label>
                <br>
                <input type="password" id="register_password" name="register_password">
                <br>
                <label for="register_contact">Contact information:</label>
                <br>
                <input type="text" id="register_contact" name="register_contact">
                <br>
                <label for="register_image">Odaberi sliku:</label>
                <br>
                <input type="file" id="register_image" name="register_image">
                <br>
                <button class="w3-button w3-round-medium w3-blue" type="submit" name="register_submit">Register</button>
            </form>
        </div>
    </div>
</div>

<script src="js/modal.js"></script>