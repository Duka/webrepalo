<?php
session_start();
include 'dbConnection.php';

if (isset($_GET['id'])) {
    $sellerId = $_GET['id'];

    $sql = "SELECT * FROM users WHERE id = $sellerId";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        $sellerResult = $result->fetch_assoc();

        $sellerListingsSql = "SELECT id, name, image, price, location FROM listings WHERE seller = $sellerId";
        $sellerListingsResult = $conn->query($sellerListingsSql);
    } else {
        echo "Listing not found.";
        exit;
    }
} else {
    echo "Invalid request.";
    exit;
}

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['update_submit'])) {
    $new_contact = $_POST['contact'];

    $target_dir = "images/users/";
    $imageFileType = strtolower(pathinfo($_FILES["image"]["name"], PATHINFO_EXTENSION));
    $target_file = $target_dir . $sellerResult['username'] . '.' . $imageFileType;

    if (!is_dir($target_dir)) {
        mkdir($target_dir, 0777, true);
    }

    if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
        $image_name = $sellerResult['username'] . '.' . $imageFileType;
    } else {
        $image_name = $sellerResult['image'];
    }

    $updateSql = "UPDATE users SET contact = '$new_contact', image = '$image_name' WHERE id = $sellerId";
    if ($conn->query($updateSql) === TRUE) {
        header("Location: userInformation.php?id=$sellerId");
        exit();
    } else {
        echo "Error: " . $updateSql . "<br>" . $conn->error;
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/x-icon" href="images/icon.png">
    <title>Korisnik <?php echo htmlspecialchars($sellerResult['username']); ?></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <?php include 'navbar.php'; ?>

        <div class="editProfileModal">
            <div class="modal-content">
                <span class="editProfileClose">&times;</span>
                <div>
                    <form method="post" action="" enctype="multipart/form-data">
                        <label for="contact">Kontakt:</label>
                        <br>
                        <input type="text" id="contact" name="contact" value="<?php echo htmlspecialchars($sellerResult['contact']); ?>">
                        <br>
                        <label for="image">Odaberi novu sliku:</label>
                        <br>
                        <input type="file" id="image" name="image">
                        <br>
                        <button class="w3-button w3-round-medium w3-blue" type="submit" name="update_submit">Ažuriraj</button>
                    </form>
                </div>
            </div>
        </div>

    <div class="sellerProfileInformation" style="margin-top: 65px">
        <div class="sellerProfile">
            <img src="images/users/<?php echo htmlspecialchars($sellerResult['image']); ?>">
            <div>
                <h3><?php echo htmlspecialchars($sellerResult['username']); ?></h3>
                <h3>Kontakt: <?php echo htmlspecialchars($sellerResult['contact']); ?></h3>
                <?php if (isset($_SESSION['user_id']) && $_SESSION['user_id'] == $sellerId): ?>
                    <button type="button" class="w3-button w3-round-medium w3-blue editProfileButton">Uredi profil</button>
                <?php endif; ?>
            </div>
        </div>
        <div class="listings">
            <?php if ($sellerListingsResult->num_rows > 0): ?>
                <?php while($row = $sellerListingsResult->fetch_assoc()): ?>
                    <a href="listing.php?id=<?php echo $row['id']; ?>">
                        <div class="listing">
                            <img src="<?php echo htmlspecialchars($row['image']); ?>">
                            <div class="listingDescription">
                                <h2><?php echo htmlspecialchars($row['name']); ?></h2>
                                <p><?php echo htmlspecialchars($row['location']); ?></p>
                                <h3><?php echo htmlspecialchars($row['price']); ?>€</h3>
                            </div>
                        </div>
                    </a>
                <?php endwhile; ?>
            <?php else: ?>
                <div style="display:flex; justify-content:center; align-items: center; background-color: white; padding: 1em;" id="nemaOglasaContainer">
                    <h2>Nema oglasa.</h2>
                </div>
            <?php endif; ?>
        </div>
    </div>


    <footer style="position: fixed; bottom: 0; width: 100%">
        <p>Created by Duka</p>
    </footer>

    <script src="js/editUserInfoModal.js"></script>
</body>
</html>

<?php $conn->close(); ?>
