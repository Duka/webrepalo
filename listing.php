<?php
session_start();
include 'dbConnection.php';

if (isset($_GET['id'])) {
    $itemId = $_GET['id'];

    $sql = "SELECT * FROM listings WHERE id = $itemId";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        $listing = $result->fetch_assoc();

        // Retrieve seller information
        $sellerId = $listing['seller'];
        $sellerSql = "SELECT * FROM users WHERE id = $sellerId";
        $sellerResult = $conn->query($sellerSql);
        if ($sellerResult->num_rows > 0) {
            $sellerInfo = $sellerResult->fetch_assoc();
        }

        $sellerListingsSql = "SELECT id, name, image, price, location FROM listings WHERE seller = $sellerId";
        $sellerListingsResult = $conn->query($sellerListingsSql);
    } else {
        echo "Listing not found.";
        exit;
    }
} else {
    echo "Invalid request.";
    exit;
}

if (isset($_POST['delete_listing'])) {
    $sql = "SELECT seller FROM listings WHERE id = $itemId";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        $listing = $result->fetch_assoc();
        if ($listing['seller'] == $_SESSION['user_id']) {
            $deleteSql = "DELETE FROM listings WHERE id = $itemId";
            if ($conn->query($deleteSql) === TRUE) {
                echo "<script>alert('Listing deleted successfully'); window.location.href='index.php';</script>";
                exit();
            } else {
                echo "Error deleting listing: " . $conn->error;
            }
        } else {
            echo "You are not authorized to delete this listing.";
        }
    } else {
        echo "Listing not found.";
    }
}

?>


<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" type="image/x-icon" href="images/icon.png">
        <title><?php echo htmlspecialchars($listing['NAME']); ?></title>
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link rel="stylesheet" href="styles.css">
    </head>

    <body>
        <?php include 'navbar.php'; ?>

        <div class="listingDetail" style="margin-top: 65px">
            <img src="<?php echo htmlspecialchars($listing['image']); ?>" alt="<?php echo htmlspecialchars($listing['NAME']); ?> image">
            <div class="listingDetailTextContainer">
                <div class="listingBasicInformation">    
                    <h2><?php echo htmlspecialchars($listing['NAME']); ?></h2>
                    <p><?php echo htmlspecialchars($listing['description']); ?></p>
                </div>
                <div class="listingExtraInformation">
                    <div>
                        <h3><?php echo htmlspecialchars($listing['price']); ?>€</h3>
                        <h3><?php echo htmlspecialchars($listing['location']); ?></h3>
                    </div>
                    <div>
                        <p>Prodavač: <?php echo htmlspecialchars($sellerInfo['username']); ?></p>
                        <p>Kontakt: <?php echo htmlspecialchars($sellerInfo['contact']); ?></p>
                    </div>
                </div>
                <?php if (isset($_SESSION['user_id']) && $listing['seller'] == $_SESSION['user_id']): ?>
                    <form method="post" action="">
                        <button type="submit" name="delete_listing" class="w3-button w3-round-medium w3-button w3-red">Obriši oglas</button>
                    </form>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <div class="sellerProfileInformation">
        <a href="userInformation.php?id=<?php echo $sellerInfo['id']; ?>">
            <div class="sellerProfile">
                <img src="images/users/<?php echo htmlspecialchars($sellerInfo['image']); ?>">
                <div>
                    <h3><?php echo htmlspecialchars($sellerInfo['username']); ?></h3>
                    <h3>Contact: <?php echo htmlspecialchars($sellerInfo['contact']); ?></h3>
                </div>
            </div>
        </a>
        <div class="listings">
            <?php if ($sellerListingsResult->num_rows > 0): ?>
                <?php while($row = $sellerListingsResult->fetch_assoc()): ?>
                    <?php if($row['id'] != $listing['id']):?>
                        <a href="listing.php?id=<?php echo $row['id']; ?>">
                            <div class="listing">
                                <img src="<?php echo htmlspecialchars($row['image']); ?>">
                                <div class="listingDescription">
                                    <h2><?php echo htmlspecialchars($row['name']); ?></h2>
                                    <p><?php echo htmlspecialchars($row['location']); ?></p>
                                    <h3><?php echo htmlspecialchars($row['price']); ?>€</h3>
                                </div>
                            </div>
                        </a>
                    <?php endif; ?>
                <?php endwhile; ?>
            <?php else: ?>
                <p>Nema oglasa.</p>
            <?php endif; ?>
        </div>
    </div>

    <script src="modal.js"></script>

    <footer>
        <p>Created by Duka</p>
    </footer>
</body>
</html>